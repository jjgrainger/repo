---
layout: post
date: 2016-11-16
title: Animate.css
link: https://daneden.github.io/animate.css/
categories: Libraries
tags: [CSS, Animation]
---

A cross-browser library of CSS animations.
