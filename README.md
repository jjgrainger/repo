# Repository

A curated collection of the best resources for web folk.

## Categories

* Showcases - Inspiration galleries (Awwwards, siteinspire, onepagelove)
* Libraries - Websites curating resources (Codyhouse, Unheap, )
* Tools - Design and development tools (Bower, Sublime Text)
* Learning - Tutorial sites and articles (Laracasts, Codecourse, Upcase, treehouse)
* Frameworks - WTF is a framework? (Bootstrap, Laravel, WordPress, Express)

---

* Articles - Blogs and magazines on web stuff (A List Apart, Smashing Magazine)
* Plugins - Stuff you plugin? (Animate.css, ACF, )
* Events - Conferences and the like
* Books - Good web books? just books?
