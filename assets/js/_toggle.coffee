(($) ->

    $('.js-toggle').on "click", (e) ->

        e.preventDefault();

        $target = $( $(this).attr 'href' )

        if ! $target.hasClass 'is-visible'
            $('.toggle.is-visible').removeClass 'is-visible'
            $('.menu-main a.is-active').removeClass 'is-active'

        $target.toggleClass 'is-visible'
        $(this).toggleClass 'is-active'

) jQuery
