(function($, window) {

    // first grab the JSON data for the properties
    $.get('/search.json', function(data) {

        console.log(data);

        // var posts = $.parseJSON(data);
        var posts = data;

        var substringMatcher = function(posts) {
          return function findMatches(q, cb) {
            var matches, substringRegex;

            // an array that will be populated with substring matches
            matches = [];

            // regex used to determine if a string contains the substring `q`
            substrRegex = new RegExp(q, 'i');

            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            $.each(posts, function(i, post) {
                var haystack = post.title;

                if(post.tags) {
                    haystack = haystack + post.tags.join(',');
                }

                if(post.category) {
                    haystack = haystack + post.category;
                }

                if (substrRegex.test(haystack)) {
                    matches.push(post);
                }
            });

            cb(matches);
          };
        };


        $('.js-typeahead').typeahead({
            hint: true,
            highlight: true,
            minLength: 3
        }, {
            name: 'posts',
            limit: 10,
            display: 'title',
            source: substringMatcher(posts),
            templates: {
                suggestion: Handlebars.compile('<div class="result post--{{category}}"><span class="result-title">{{title}} <span class="result-category">{{category}}</span></span><div class="result-content">{{content}}</div><span class="result-meta">{{link}}</div>')
            }
        }).bind('typeahead:selected', function (obj, posts) {
            window.location.href = posts.link;
        });


    });

})(jQuery, window);
